/**
 * Created by mitfity on 30.03.2016.
 */

/**
 * Factory to manage CRUD operations with RSS-channels. (Singleton)
 * @type {{getInstance}}
 */
var CIChannelFactory = (function () {
    var instance;

    function init() {
        var channelList = null;

        return {

            /**
             * Fetches channels array from server. On success updates channelList array.
             * @param success (function) Callback on success
             */
            getChannels: function (success) {
                $.ajax({
                    url: '/api/channels',
                    dataType: 'json',
                    cache: false,
                    success: function (data) {
                        channelList = data;

                        if (typeof success === 'function') {
                            success(data);
                        }
                    },
                    error: function (xhr, status, err) {
                        console.error(this.url, status, err.toString());
                    }
                });
            },

            /**
             * Fetches channels array from server and looks for channel with passed id in array. On success updates
             * channelList array.
             * @param id (integer) Channel id
             * @param success (function) Callback on success
             * @returns {boolean}
             */
            getChannel: function (id, success) {
                if (typeof id === 'undefined') {
                    return false;
                }

                // Update channelList to match server data and after that check if there are channels
                this.getChannels(function (data) {
                    if (typeof channelList !== 'undefined' && channelList.length > 0) {
                        var isChannelFound = channelList.some(function (channel, index, collection) {
                            if (channel.id === id) {
                                if (typeof success === 'function') {
                                    success(channel);
                                }
                                return true;
                            }
                            return false;
                        });

                        if (!isChannelFound) {
                            console.log('(CIError): no such channel');
                        }
                    } else {
                        console.log('(CIError): empty array');
                    }
                });
                return true;
            },

            /**
             * Adds new channel via POST request to server. On success updates channelList array.
             * @param channelUrl (String) Link to RSS-channel
             * @param success (function) Callback on success
             */
            addChannel: function (channelUrl, success) {
                if (channelUrl === 'undefined') {
                    return;
                }

                var channel = {
                    id: Date.now(),
                    url: channelUrl
                };

                $.ajax({
                    url: '/api/channels',
                    dataType: 'json',
                    type: 'POST',
                    data: channel,
                    success: function (data) {
                        channelList = data;

                        if (typeof success === 'function') {
                            success(data);
                        }
                    },
                    error: function (xhr, status, err) {
                        console.error(this.url, status, err.toString());
                    }
                });
            },

            /**
             * Removes channel via DELETE request to server. On success updates channelList array.
             * @param id
             * @param success
             * @returns {boolean}
             */
            removeChannel: function (id, success) {
                if (typeof id === 'undefined') {
                    return false;
                }

                var requestData = {
                    id: id
                };

                $.ajax({
                    url: '/api/channels',
                    dataType: 'json',
                    type: 'DELETE',
                    data: requestData,
                    success: function (response) {
                        channelList = response;

                        if (typeof success === 'function') {
                            success(response);
                        }
                    },
                    error: function (xhr, status, err) {
                        console.error(this.url, status, err.toString());
                    }
                });
            }
        };
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();