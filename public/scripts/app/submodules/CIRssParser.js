/**
 * Created by mitfity on 11.04.2016.
 */

/**
 * Rss-parser to get json object from xml by url.
 * @type {{getInstance}}
 */
var CIRssParser = (function () {
    var instance;

    function init() {

        function getRss(url, success) {
            $.ajax({
                url: '/api/getRss?url=' + url,
                dataType: "xml",
                type: 'GET',
                data: null,
                success: function(xml) {
                    if (typeof success === 'function') {
                        success(xml);
                    }
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(url, status, err.toString());
                }
            });
        }

        function getHtml(url, success) {
            $.ajax({
                url: '/api/getHtml?url=' + url,
                dataType: "html",
                type: 'GET',
                data: null,
                success: function(xml) {
                    if (typeof success === 'function') {
                        success(xml);
                    }
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(url, status, err.toString());
                }
            });
        }

        function parseRss(xml, success) {
            var feed = new RssFeed(),
                channel = $('channel', xml).eq(0),
                counter = 0;

            if ($('rss', xml).length == 0) {
                feed.version = '1.0';
            }
            else {
                feed.version = $('rss', xml).eq(0).attr('version');
            }

            feed.title = $(channel).find('title:first').text();
            feed.link = $(channel).find('link:first').text();
            feed.description = $(channel).find('description:first').text();
            feed.language = $(channel).find('language:first').text();
            feed.updated = $(channel).find('lastBuildDate:first').text();

            feed.items = [];

            $('item', xml).each( function() {
                var item = new FeedItem();

                item.title = $(this).find('title').eq(0).text();
                item.link = $(this).find('link').eq(0).text();
                item.description = $(this).find('description').eq(0).text();
                item.updated = $(this).find('pubDate').eq(0).text();
                item.id = $(this).find('guid').eq(0).text()  + counter++ || item.link + counter++;

                feed.items.push(item);
            });

            if (typeof success === 'function') {
                success(feed);
            }
        }

        function parseHtml(html, success) {
            var feedItemContent = {},
                $html = $(html);

            feedItemContent.title = html.split("<title")[1].split(">").slice(1).join(">").split("</title>")[0];
            feedItemContent.content = null;

            if ($html.find('*[property~="articleBody"]').length) {
                feedItemContent.content = $html.find('*[property~="articleBody"]').get(0);
            } else if ($html.find('*[itemprop~="articleBody"]').length) {
                feedItemContent.content = $html.find('*[itemprop~="articleBody"]').get(0);
            } else if ($html.find('*[itemprop~="associatedMedia"]').length) {
                feedItemContent.content = $html.find('*[itemprop~="associatedMedia"]').get(0);
            } else if ($html.find('*[id="storytext"]').length) {
                feedItemContent.content = $html.find('*[id="storytext"]').get(0);
            } else if ($html.find('*[id="story_text"]').length) {
                feedItemContent.content = $html.find('*[id="story_text"]').get(0);
            } else if ($html.find('.post-content').length) {
                feedItemContent.content = $html.find('.post-content').get(0);
            } else if ($html.find('.story-body').length) {
                feedItemContent.content = $html.find('.story-body').get(0);
            } else if ($html.find('.articleBody').length) {
                feedItemContent.content = $html.find('.articleBody').get(0);
            } else if ($html.find('.el__video-collection__meta-wrapper').length) {
                feedItemContent.content = $html.find('.el__video-collection__meta-wrapper').get(0);
            } else if ($html.find('article > .l-container').length) {
                feedItemContent.content = $html.find('article > .l-container').get(0);
            } else if ($html.find('.post-txt').length) {
                feedItemContent.content = $html.find('.post-txt').get(0);
            }

            if (typeof success === 'function') {
                success(feedItemContent);
            }
        }

        return {

            /**
             * Gets xml from url, parses it and returns javascript object.
             * @param url Link to RSS to parse
             * @param success Function callback that returns javascript object of RSS
             */
            parse: function (url, success) {
                getRss(url, function (xml) {
                    parseRss(xml, success);
                });
            },

            /**
             * Gets html from url, parses it and returns javascript object.
             * @param url Link to HTML to parse
             * @param success Function callback
             */
            parseHtml: function (url, success) {
                getHtml(url, function (html) {
                    parseHtml(html, success);
                });
            }
        };
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();