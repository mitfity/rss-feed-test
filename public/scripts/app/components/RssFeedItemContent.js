var RssFeedItemContent = React.createClass({

    componentDidUpdate() {
        ReactDOM.findDOMNode(this.refs.overflowWrapper).scrollTop = 0;
    },

    render: function () {
        return (
            <div className="rss-feed-item-content rss-feed-section">
                <div className="overflow-wrapper" ref="overflowWrapper">
                    <h2>{this.props.feedItem ? this.props.feedItem.title : null}</h2>
                    <div dangerouslySetInnerHTML={{__html: this.props.feedItem ? this.props.feedItem.content.innerHTML : null}}></div>
                </div>
            </div>
        );
    }
});