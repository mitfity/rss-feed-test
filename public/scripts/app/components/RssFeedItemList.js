var RssFeedItemList = React.createClass({

    getInitialState: function () {
        return {
            selectedItem: null
        };
    },

    onFeedItemSelected: function (feedItem, e) {
        this.setState({
            selectedItem: feedItem
        });

        if (typeof this.props.onFeedItemSelected === 'function') {
            this.props.onFeedItemSelected(feedItem);
        }
    },

    render: function () {
        var feedItemListNodes = null;

        if (this.props.feed) {
            feedItemListNodes = this.props.feed.items.map(function (item) {
                var isSelected = (this.state.selectedItem && this.state.selectedItem.id === item.id);

                return (
                    <li key={item.id} onClick={this.onFeedItemSelected.bind(this, item)}
                        className={isSelected ? "selected" : null}>
                        <a href="#">{item.title}</a>
                    </li>
                );
            }.bind(this));
        }

        return (
            <div className="rss-feed-item-list rss-feed-section">
                <h2>
                    <span className="link">{this.props.feed ? this.props.feed.link : null}</span>
                    <span className="feeds-count" hidden={this.props.feed ? false : true}>{this.props.feed ? this.props.feed.items.length : null}</span>
                </h2>
                <ul className="styled-list">{feedItemListNodes ? feedItemListNodes : null}</ul>
            </div>
        );
    }
});