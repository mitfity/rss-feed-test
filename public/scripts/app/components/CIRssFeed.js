var CIRssFeed = React.createClass({

    getInitialState: function() {
        return {
            selectedFeed: null,
            selectedFeedItem: null
        };
    },

    onChannelSelected: function (channel) {
        var rssParser = CIRssParser.getInstance();

        rssParser.parse(channel.url, function (feed) {
            this.setState({
                selectedFeed: feed
            });
        }.bind(this));
    },

    onFeedItemSelected: function (feedItem) {
        var rssParser = CIRssParser.getInstance();

        if (this.state.selectedFeedItem && this.state.selectedFeedItem.id === feedItem.id) {
            return;
        }

        rssParser.parseHtml(feedItem.link, function (feedItemContent) {
            feedItemContent.id = feedItem.id;
            this.setState({
                selectedFeedItem: feedItemContent
            });
        }.bind(this));
    },

    render: function () {
        return (
            <div className="rss-feed">
                <h1>RSS Feed</h1>
                <div className="rss-feed-sections-container">
                    <RssChannelList onChannelSelected={this.onChannelSelected} />
                    <RssFeedItemList feed={this.state.selectedFeed} onFeedItemSelected={this.onFeedItemSelected} />
                    <RssFeedItemContent feedItem={this.state.selectedFeedItem} />
                    <RssFeedItemDiagram feedItem={this.state.selectedFeedItem} />
                </div>
            </div>
        );
    }
});