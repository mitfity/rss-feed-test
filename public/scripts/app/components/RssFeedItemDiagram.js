var RssFeedItemDiagram = React.createClass({

    getInitialState: function() {
        return {
            reRenderChart: false
        };
    },

    componentDidUpdate: function() {
        if (this.state.reRenderChart) {
            this.refs.pieChart.fc_chart.setChartData(this.getChartDataSource(), "json");

            this.setState({
                reRenderChart: false
            });
        }
    },

    componentWillReceiveProps: function(newProps) {
        if (this.props.feedItem && newProps.feedItem && this.props.feedItem.id === newProps.feedItem.id) {
            return;
        }

        this.setState({
            reRenderChart: true
        });
    },

    /**
     * Generates new data source due to passed properties to component.
     * @returns {Object} FusionCharts data source
     */
    getChartDataSource: function () {
        var dataSource = {
                chart: {
                    caption: "Percentage of letters in text",
                    startingangle: "120",
                    showlabels: "0",
                    showlegend: "1",
                    enablemultislicing: "0",
                    slicingdistance: "15",
                    showpercentvalues: "1",
                    showpercentintooltip: "0",
                    plottooltext: "Letter : <strong>\"$label\"</strong> Total visit : <strong>$datavalue</strong>"
                },
                data: []
            },
            text = this.props.feedItem ? this.props.feedItem.content.textContent : null,
            alphabet = "abcdefghijklmnopqrstuvwxyz";

        if (text) {
            var letterMatrix = [];

            for (var i = 0; i < text.length; i++) {
                var char = text[i].toLowerCase();

                // Check if it is a letter
                if (alphabet.indexOf(char) > -1) {
                    if (!letterMatrix[char]) {
                        letterMatrix[char] = {};
                        letterMatrix[char].count = 1;
                    } else {
                        letterMatrix[char].count++;
                    }
                }
            }

            for (var key in letterMatrix) {
                if (letterMatrix.hasOwnProperty(key)) {
                    dataSource.data.push({
                        label: key,
                        value: letterMatrix[key].count
                    });
                }
            }
        }

        return dataSource;
    },

    /**
     * Generates initialization chart config
     * @returns {Object}
     */
    getChartConfig: function () {
        return {
            id: "pie-chart",
            renderAt: "pie-chart-container",
            type: "pie2d",
            width: 400,
            height: 400,
            dataFormat: "json",
            dataSource: []
        };
    },

    render: function () {
        return (
            <div className="rss-feed-item-diagram rss-feed-section">
                <div className="overflow-wrapper">
                    <react_fc.FusionCharts {...this.getChartConfig()} ref="pieChart" />
                </div>
            </div>
        );
    }
});