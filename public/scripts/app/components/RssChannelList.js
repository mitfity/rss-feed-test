var RssChannelList = React.createClass({

    loadChannelsFromServer: function () {
        var channelFactory = CIChannelFactory.getInstance();

        channelFactory.getChannels(function (channels) {
            this.setState({data: channels});
        }.bind(this));
    },

    getInitialState: function () {
        return {
            data: [],
            url: '',
            selectedChannel: null
        };
    },

    componentDidMount: function () {
        this.loadChannelsFromServer();
    },

    onUrlChange: function (e) {
        this.setState({
            url: e.target.value
        });
    },

    onAddChannelSubmit: function (e) {
        e.preventDefault();
        var url = this.state.url.trim();

        if (!url) {
            return;
        }

        var channelFactory = CIChannelFactory.getInstance();
        channelFactory.addChannel(url, function (channelList) {
            this.setState({
                data: channelList
            });
        }.bind(this));

        this.setState({
            url: ''
        });
    },

    onChannelSelected: function (channel, e) {
        this.setState({
            selectedChannel: channel
        });

        if (typeof this.props.onChannelSelected === 'function') {
            this.props.onChannelSelected(channel);
        }
    },

    render: function () {
        var channelListNodes = this.state.data.map(function (channel) {
            var isSelected = (this.state.selectedChannel && this.state.selectedChannel.id === channel.id);

            return (
                <li key={channel.id} onClick={this.onChannelSelected.bind(this, channel)}
                    className={isSelected ? "selected" : null}>
                    <a href="#">{channel.url}</a>
                </li>
            );
        }.bind(this));

        return (
            <div className="rss-channel-list rss-feed-section">
                <form className="channelForm" onSubmit={this.onAddChannelSubmit}>
                    <input
                        type="text"
                        placeholder="RSS url"
                        value={this.state.url}
                        onChange={this.onUrlChange}
                    />
                    <input type="submit" value="Add" />
                    <span className="channel-count">{this.state.data.length}</span>
                </form>
                <ul className="styled-list">{channelListNodes}</ul>
            </div>
        );
    }
});