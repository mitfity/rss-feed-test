var CIRssFeed = React.createClass({

    getInitialState: function() {
        return {
            selectedFeed: null,
            selectedFeedItem: null
        };
    },

    onChannelSelected: function (channel) {
        var rssParser = CIRssParser.getInstance();

        rssParser.parse(channel.url, function (feed) {
            this.setState({
                selectedFeed: feed
            });
        }.bind(this));
    },

    onFeedItemSelected: function (feedItem) {
        var rssParser = CIRssParser.getInstance();

        if (this.state.selectedFeedItem && this.state.selectedFeedItem.id === feedItem.id) {
            return;
        }

        rssParser.parseHtml(feedItem.link, function (feedItemContent) {
            feedItemContent.id = feedItem.id;
            this.setState({
                selectedFeedItem: feedItemContent
            });
        }.bind(this));
    },

    render: function () {
        return (
            <div className="rss-feed">
                <h1>RSS Feed</h1>
                <div className="rss-feed-sections-container">
                    <RssChannelList onChannelSelected={this.onChannelSelected} />
                    <RssFeedItemList feed={this.state.selectedFeed} onFeedItemSelected={this.onFeedItemSelected} />
                    <RssFeedItemContent feedItem={this.state.selectedFeedItem} />
                    <RssFeedItemDiagram feedItem={this.state.selectedFeedItem} />
                </div>
            </div>
        );
    }
});
var RssChannelList = React.createClass({

    loadChannelsFromServer: function () {
        var channelFactory = CIChannelFactory.getInstance();

        channelFactory.getChannels(function (channels) {
            this.setState({data: channels});
        }.bind(this));
    },

    getInitialState: function () {
        return {
            data: [],
            url: '',
            selectedChannel: null
        };
    },

    componentDidMount: function () {
        this.loadChannelsFromServer();
    },

    onUrlChange: function (e) {
        this.setState({
            url: e.target.value
        });
    },

    onAddChannelSubmit: function (e) {
        e.preventDefault();
        var url = this.state.url.trim();

        if (!url) {
            return;
        }

        var channelFactory = CIChannelFactory.getInstance();
        channelFactory.addChannel(url, function (channelList) {
            this.setState({
                data: channelList
            });
        }.bind(this));

        this.setState({
            url: ''
        });
    },

    onChannelSelected: function (channel, e) {
        this.setState({
            selectedChannel: channel
        });

        if (typeof this.props.onChannelSelected === 'function') {
            this.props.onChannelSelected(channel);
        }
    },

    render: function () {
        var channelListNodes = this.state.data.map(function (channel) {
            var isSelected = (this.state.selectedChannel && this.state.selectedChannel.id === channel.id);

            return (
                <li key={channel.id} onClick={this.onChannelSelected.bind(this, channel)}
                    className={isSelected ? "selected" : null}>
                    <a href="#">{channel.url}</a>
                </li>
            );
        }.bind(this));

        return (
            <div className="rss-channel-list rss-feed-section">
                <form className="channelForm" onSubmit={this.onAddChannelSubmit}>
                    <input
                        type="text"
                        placeholder="RSS url"
                        value={this.state.url}
                        onChange={this.onUrlChange}
                    />
                    <input type="submit" value="Add" />
                    <span className="channel-count">{this.state.data.length}</span>
                </form>
                <ul className="styled-list">{channelListNodes}</ul>
            </div>
        );
    }
});
var RssFeedItemContent = React.createClass({

    componentDidUpdate() {
        ReactDOM.findDOMNode(this.refs.overflowWrapper).scrollTop = 0;
    },

    render: function () {
        return (
            <div className="rss-feed-item-content rss-feed-section">
                <div className="overflow-wrapper" ref="overflowWrapper">
                    <h2>{this.props.feedItem ? this.props.feedItem.title : null}</h2>
                    <div dangerouslySetInnerHTML={{__html: this.props.feedItem ? this.props.feedItem.content.innerHTML : null}}></div>
                </div>
            </div>
        );
    }
});
var RssFeedItemDiagram = React.createClass({

    getInitialState: function() {
        return {
            reRenderChart: false
        };
    },

    componentDidUpdate: function() {
        if (this.state.reRenderChart) {
            this.refs.pieChart.fc_chart.setChartData(this.getChartDataSource(), "json");

            this.setState({
                reRenderChart: false
            });
        }
    },

    componentWillReceiveProps: function(newProps) {
        if (this.props.feedItem && newProps.feedItem && this.props.feedItem.id === newProps.feedItem.id) {
            return;
        }

        this.setState({
            reRenderChart: true
        });
    },

    /**
     * Generates new data source due to passed properties to component.
     * @returns {Object} FusionCharts data source
     */
    getChartDataSource: function () {
        var dataSource = {
                chart: {
                    caption: "Percentage of letters in text",
                    startingangle: "120",
                    showlabels: "0",
                    showlegend: "1",
                    enablemultislicing: "0",
                    slicingdistance: "15",
                    showpercentvalues: "1",
                    showpercentintooltip: "0",
                    plottooltext: "Letter : <strong>\"$label\"</strong> Total visit : <strong>$datavalue</strong>"
                },
                data: []
            },
            text = this.props.feedItem ? this.props.feedItem.content.textContent : null,
            alphabet = "abcdefghijklmnopqrstuvwxyz";

        if (text) {
            var letterMatrix = [];

            for (var i = 0; i < text.length; i++) {
                var char = text[i].toLowerCase();

                // Check if it is a letter
                if (alphabet.indexOf(char) > -1) {
                    if (!letterMatrix[char]) {
                        letterMatrix[char] = {};
                        letterMatrix[char].count = 1;
                    } else {
                        letterMatrix[char].count++;
                    }
                }
            }

            for (var key in letterMatrix) {
                if (letterMatrix.hasOwnProperty(key)) {
                    dataSource.data.push({
                        label: key,
                        value: letterMatrix[key].count
                    });
                }
            }
        }

        return dataSource;
    },

    /**
     * Generates initialization chart config
     * @returns {Object}
     */
    getChartConfig: function () {
        return {
            id: "pie-chart",
            renderAt: "pie-chart-container",
            type: "pie2d",
            width: 400,
            height: 400,
            dataFormat: "json",
            dataSource: []
        };
    },

    render: function () {
        return (
            <div className="rss-feed-item-diagram rss-feed-section">
                <div className="overflow-wrapper">
                    <react_fc.FusionCharts {...this.getChartConfig()} ref="pieChart" />
                </div>
            </div>
        );
    }
});
var RssFeedItemList = React.createClass({

    getInitialState: function () {
        return {
            selectedItem: null
        };
    },

    onFeedItemSelected: function (feedItem, e) {
        this.setState({
            selectedItem: feedItem
        });

        if (typeof this.props.onFeedItemSelected === 'function') {
            this.props.onFeedItemSelected(feedItem);
        }
    },

    render: function () {
        var feedItemListNodes = null;

        if (this.props.feed) {
            feedItemListNodes = this.props.feed.items.map(function (item) {
                var isSelected = (this.state.selectedItem && this.state.selectedItem.id === item.id);

                return (
                    <li key={item.id} onClick={this.onFeedItemSelected.bind(this, item)}
                        className={isSelected ? "selected" : null}>
                        <a href="#">{item.title}</a>
                    </li>
                );
            }.bind(this));
        }

        return (
            <div className="rss-feed-item-list rss-feed-section">
                <h2>
                    <span className="link">{this.props.feed ? this.props.feed.link : null}</span>
                    <span className="feeds-count" hidden={this.props.feed ? false : true}>{this.props.feed ? this.props.feed.items.length : null}</span>
                </h2>
                <ul className="styled-list">{feedItemListNodes ? feedItemListNodes : null}</ul>
            </div>
        );
    }
});
/**
 * Created by mitfity on 30.03.2016.
 */

/**
 * Factory to manage CRUD operations with RSS-channels. (Singleton)
 * @type {{getInstance}}
 */
var CIChannelFactory = (function () {
    var instance;

    function init() {
        var channelList = null;

        return {

            /**
             * Fetches channels array from server. On success updates channelList array.
             * @param success (function) Callback on success
             */
            getChannels: function (success) {
                $.ajax({
                    url: '/api/channels',
                    dataType: 'json',
                    cache: false,
                    success: function (data) {
                        channelList = data;

                        if (typeof success === 'function') {
                            success(data);
                        }
                    },
                    error: function (xhr, status, err) {
                        console.error(this.url, status, err.toString());
                    }
                });
            },

            /**
             * Fetches channels array from server and looks for channel with passed id in array. On success updates
             * channelList array.
             * @param id (integer) Channel id
             * @param success (function) Callback on success
             * @returns {boolean}
             */
            getChannel: function (id, success) {
                if (typeof id === 'undefined') {
                    return false;
                }

                // Update channelList to match server data and after that check if there are channels
                this.getChannels(function (data) {
                    if (typeof channelList !== 'undefined' && channelList.length > 0) {
                        var isChannelFound = channelList.some(function (channel, index, collection) {
                            if (channel.id === id) {
                                if (typeof success === 'function') {
                                    success(channel);
                                }
                                return true;
                            }
                            return false;
                        });

                        if (!isChannelFound) {
                            console.log('(CIError): no such channel');
                        }
                    } else {
                        console.log('(CIError): empty array');
                    }
                });
                return true;
            },

            /**
             * Adds new channel via POST request to server. On success updates channelList array.
             * @param channelUrl (String) Link to RSS-channel
             * @param success (function) Callback on success
             */
            addChannel: function (channelUrl, success) {
                if (channelUrl === 'undefined') {
                    return;
                }

                var channel = {
                    id: Date.now(),
                    url: channelUrl
                };

                $.ajax({
                    url: '/api/channels',
                    dataType: 'json',
                    type: 'POST',
                    data: channel,
                    success: function (data) {
                        channelList = data;

                        if (typeof success === 'function') {
                            success(data);
                        }
                    },
                    error: function (xhr, status, err) {
                        console.error(this.url, status, err.toString());
                    }
                });
            },

            /**
             * Removes channel via DELETE request to server. On success updates channelList array.
             * @param id
             * @param success
             * @returns {boolean}
             */
            removeChannel: function (id, success) {
                if (typeof id === 'undefined') {
                    return false;
                }

                var requestData = {
                    id: id
                };

                $.ajax({
                    url: '/api/channels',
                    dataType: 'json',
                    type: 'DELETE',
                    data: requestData,
                    success: function (response) {
                        channelList = response;

                        if (typeof success === 'function') {
                            success(response);
                        }
                    },
                    error: function (xhr, status, err) {
                        console.error(this.url, status, err.toString());
                    }
                });
            }
        };
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();
/**
 * Created by mitfity on 11.04.2016.
 */

/**
 * Rss-parser to get json object from xml by url.
 * @type {{getInstance}}
 */
var CIRssParser = (function () {
    var instance;

    function init() {

        function getRss(url, success) {
            $.ajax({
                url: '/api/getRss?url=' + url,
                dataType: "xml",
                type: 'GET',
                data: null,
                success: function(xml) {
                    if (typeof success === 'function') {
                        success(xml);
                    }
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(url, status, err.toString());
                }
            });
        }

        function getHtml(url, success) {
            $.ajax({
                url: '/api/getHtml?url=' + url,
                dataType: "html",
                type: 'GET',
                data: null,
                success: function(xml) {
                    if (typeof success === 'function') {
                        success(xml);
                    }
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(url, status, err.toString());
                }
            });
        }

        function parseRss(xml, success) {
            var feed = new RssFeed(),
                channel = $('channel', xml).eq(0),
                counter = 0;

            if ($('rss', xml).length == 0) {
                feed.version = '1.0';
            }
            else {
                feed.version = $('rss', xml).eq(0).attr('version');
            }

            feed.title = $(channel).find('title:first').text();
            feed.link = $(channel).find('link:first').text();
            feed.description = $(channel).find('description:first').text();
            feed.language = $(channel).find('language:first').text();
            feed.updated = $(channel).find('lastBuildDate:first').text();

            feed.items = [];

            $('item', xml).each( function() {
                var item = new FeedItem();

                item.title = $(this).find('title').eq(0).text();
                item.link = $(this).find('link').eq(0).text();
                item.description = $(this).find('description').eq(0).text();
                item.updated = $(this).find('pubDate').eq(0).text();
                item.id = $(this).find('guid').eq(0).text()  + counter++ || item.link + counter++;

                feed.items.push(item);
            });

            if (typeof success === 'function') {
                success(feed);
            }
        }

        function parseHtml(html, success) {
            var feedItemContent = {},
                $html = $(html);

            feedItemContent.title = html.split("<title")[1].split(">").slice(1).join(">").split("</title>")[0];
            feedItemContent.content = null;

            if ($html.find('*[property~="articleBody"]').length) {
                feedItemContent.content = $html.find('*[property~="articleBody"]').get(0);
            } else if ($html.find('*[itemprop~="articleBody"]').length) {
                feedItemContent.content = $html.find('*[itemprop~="articleBody"]').get(0);
            } else if ($html.find('*[itemprop~="associatedMedia"]').length) {
                feedItemContent.content = $html.find('*[itemprop~="associatedMedia"]').get(0);
            } else if ($html.find('*[id="storytext"]').length) {
                feedItemContent.content = $html.find('*[id="storytext"]').get(0);
            } else if ($html.find('*[id="story_text"]').length) {
                feedItemContent.content = $html.find('*[id="story_text"]').get(0);
            } else if ($html.find('.post-content').length) {
                feedItemContent.content = $html.find('.post-content').get(0);
            } else if ($html.find('.story-body').length) {
                feedItemContent.content = $html.find('.story-body').get(0);
            } else if ($html.find('.articleBody').length) {
                feedItemContent.content = $html.find('.articleBody').get(0);
            } else if ($html.find('.el__video-collection__meta-wrapper').length) {
                feedItemContent.content = $html.find('.el__video-collection__meta-wrapper').get(0);
            } else if ($html.find('article > .l-container').length) {
                feedItemContent.content = $html.find('article > .l-container').get(0);
            } else if ($html.find('.post-txt').length) {
                feedItemContent.content = $html.find('.post-txt').get(0);
            }

            if (typeof success === 'function') {
                success(feedItemContent);
            }
        }

        return {

            /**
             * Gets xml from url, parses it and returns javascript object.
             * @param url Link to RSS to parse
             * @param success Function callback that returns javascript object of RSS
             */
            parse: function (url, success) {
                getRss(url, function (xml) {
                    parseRss(xml, success);
                });
            },

            /**
             * Gets html from url, parses it and returns javascript object.
             * @param url Link to HTML to parse
             * @param success Function callback
             */
            parseHtml: function (url, success) {
                getHtml(url, function (html) {
                    parseHtml(html, success);
                });
            }
        };
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();
/**
 * Created by mitfity on 11.04.2016.
 */

function FeedItem() {}

FeedItem.prototype = {
    title: '',
    link: '',
    description: '',
    updated: '',
    id: ''
};
/**
 * Created by mitfity on 11.04.2016.
 */

function RssFeed() {}

RssFeed.prototype = {

    type: '',
    version: '',
    title: '',
    link: '',
    description: '',
    updated: '',
    feedItems: null
};
(function () {

    ReactDOM.render(
        <CIRssFeed />,
        document.getElementById('content')
    );

})();
