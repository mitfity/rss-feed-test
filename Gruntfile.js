/**
 * Created by mitfity on 28.03.2016.
 */

module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat :{
            options: {
                separator: '\n'
            },
            mainApp: {
                src: [
                    'public/scripts/app/**/*.js',
                    '!public/scripts/app/main.js',
                    'public/scripts/app/main.js'
                ],
                dest: 'public/scripts/<%= pkg.name %>.js'
            }
        },
        watch: {
            jsx: {
                files: ['<%= concat.mainApp.src %>'],
                tasks: ['concat']
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['concat']);

};