<?php

$scriptInvokedFromCli = isset($_SERVER['argv'][0]) && $_SERVER['argv'][0] === 'index.php';

if ($scriptInvokedFromCli) {
    $port = getenv('PORT');
    if (empty($port)) {
        $port = "3000";
    }
    
    echo 'starting server on port '. $port . PHP_EOL;
    exec('php -S localhost:'. $port . ' -t public index.php');
} else {
    return routeRequest();
}

function routeRequest()
{
    $uri = $_SERVER['REQUEST_URI'];
    
    if ($uri == '/') {
        echo file_get_contents('./public/index.html');
    } elseif (preg_match('/\/api\/channel(\?.*)?/', $uri)) {
        $channels = file_get_contents('channels.json');

        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $channelsDecoded = json_decode($channels, true);

            foreach ($channelsDecoded as $channel) {
                if ($channel['url'] === $_POST['url']) {
                    echo 'Channel already exists';
                    return false;
                }
            }

            $channelsDecoded[] = [
                'id'   => round(microtime(true) * 1000),
                'url'  => $_POST['url']
            ];

            $channels = json_encode($channelsDecoded, JSON_PRETTY_PRINT);
            file_put_contents('channels.json', $channels);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
            $deleteData = file_get_contents("php://input");
            $explodedData = explode('&', $deleteData);

            $_DELETE = [];
            foreach($explodedData as $pair) {
                $item = explode('=', $pair);
                if(count($item) == 2) {
                    $_DELETE[urldecode($item[0])] = urldecode($item[1]);
                }
            }
            $targetId = (float)$_DELETE['id'];

            $channelsDecoded = json_decode($channels, true);
            for ($i = 0; $i < count($channelsDecoded); $i++) {
                if ($channelsDecoded[$i]['id'] === $targetId) {
                    array_splice($channelsDecoded, $i, 1);
                    break;
                }
            }

            $channels = json_encode($channelsDecoded, JSON_PRETTY_PRINT);
            file_put_contents('channels.json', $channels);
        }
        header('Content-Type: application/json');
        header('Cache-Control: no-cache');
        header('Access-Control-Allow-Origin: *');

        echo $channels;
    } elseif (preg_match('/\/api\/getRss(\?.*)?/', $uri)) {
        header('Content-type: application/xml');
        $handle = fopen($_REQUEST['url'], "r");
        if ($handle) {
            while (!feof($handle)) {
                $buffer = fgets($handle, 4096);
                echo $buffer;
            }
            fclose($handle);
        }
    } elseif (preg_match('/\/api\/getHtml(\?.*)?/', $uri)) {
        header('Content-type: text/html');
        $html = file_get_contents($_REQUEST['url']);
        echo $html;
    } else {
        return false;
    }
}
